from neo4j import GraphDatabase
import copy
driver = GraphDatabase.driver('bolt://localhost:7687', auth=("neo4j", "1234"))

def create_tender(title, description, amount, price, days):
    session = driver.session()
    query = '''
    create (n:tender {id:apoc.create.uuid(), title:$title ,
     description:$description, amount:$amount, price:$price, visit_number:0 ,
     exp_date:Datetime()+Duration({days:$days}), create_at:datetime()}) return n.id as id
    '''
    result = session.run(query,title=title,description=description,amount=amount
    ,price=price,days=days)
    data = []
    for each in result:
        data=each['id']
    session.close()
    return data


def search_single_user_phone_number(phone_number):
    if phone_number[0] =='0':
        phone_number = phone_number.replace("0","+98",1)

    session = driver.session()
    query = '''
    match (n:user {phone_number:$phone_number}) return n
    '''
    result = session.run(query,phone_number=phone_number)
    data = []
    for each in result:
        data = each['n']
    session.close()
    return data


def create_user(phone_number, password):
    if phone_number[0] =='0':
        phone_number = phone_number.replace("0","+98",1)
    session = driver.session()
    query = '''
    create (n:user {id:apoc.create.uuid(),phone_number:$phone_number,
    password:$password, create_at:datetime()}) return n
    '''
    result = session.run(query,phone_number=phone_number, password=password)
    data = result.data()
    session.close()
    return data

def get_user_by_id(id):
    session = driver.session()
    query = '''
    match (n:user {id:$id}) return n
    '''
    result = session.run(query,id=id)
    data = result.data()
    session.close()
    return data


def connect_two_nodes_by_id(id1, id2):
    session = driver.session()
    query = '''
    match (first_node {id:$id1}),(second_node {id:$id2})
    create (first_node)-[r:rel {id:apoc.create.uuid()
    ,create_at:datetime()}]->(second_node)
    '''
    session.run(query,id1=id1,id2=id2)
    session.close()


def show_user_tenders(user_id):
    session = driver.session()
    query = '''
    match (user {id:$id})-[]->(tender:tender )
    where datetime() < tender.exp_date and not tender:deactivated
    with duration.indays(datetime(),datetime(tender.exp_date)).days as exp_date ,tender
    return tender,exp_date
    '''
        # set tender.exp_date = duration.indays(date(datetime()) , date(tender.exp_date) ).days

    result = session.run(query,id=user_id)
    data = result.data()
    session.close()
    return data

def get_single_tender(tender_id):
    session = driver.session()
    query = '''
    match (tender:tender {id:$tender_id})<-[]-(user:user)
    where tender.exp_date > datetime() and not tender:deactivated
    with duration.between(datetime(),datetime(tender.exp_date)).days as exp_date,tender,user
    return tender,exp_date,user
    '''
    result = session.run(query,tender_id=tender_id)
    data = result.data()
    session.close()
    return data

def add_visit_to_tender(tender_id):
    # todo:: // have to complete this section
    return True

def deactivate_tender(tender_id):
    session = driver.session()
    query = '''
    match (tender:tender {id:$tender_id})
    set tender :deactivated
    '''
    result = session.run(query,tender_id=tender_id)
    session.close()
    return True

def get_tenders(limit:int=10,skip:int=20):
    session = driver.session()
    query =    '''
    match (tender:tender )
    where not tender:deactivated and tender.exp_date > datetime()
    with duration.between(datetime(),datetime(tender.exp_date)).days as exp_date,tender
    return tender,exp_date
    order by tender.create_at
    skip $skip limit $limit
    '''
    result = session.run(query,skip=skip, limit=limit)
    data = result.data()
    session.close()
    return data


def create_auction(title,description,amount,price,days,node_type:str="auction"):
    session = driver.session()
    query = '''
    create (n:%(node_type)s {id:apoc.create.uuid(), title:$title ,
     description:$description, amount:$amount, price:$price, visit_number:0 ,
     exp_date:Datetime()+Duration({days:$days}), create_at:datetime()}) return n.id as id
    '''
    query = query % {"node_type":node_type}
    result = session.run(query,title=title,description=description,amount=amount
    ,price=price,days=days)
    data = []
    for each in result:
        data=each['id']
    session.close()
    return data
