from flask import Flask
# from flask import current_app as app
from flask import render_template, request , make_response ,g
from functools import wraps

import neo4j_service as db
app = Flask(__name__)


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        my_cookie = request.cookies.get('token')
        if not my_cookie:
            return render_template("log_in.html")
        user = db.get_user_by_id(my_cookie)
        if len(user) == 0:
            return render_template("log_in.html")
        g.user = user[0]['n']
        print(g.user)
        return f(*args, **kwargs)
    return decorator

@app.route('/')
def index():
    return render_template(
        'index.html'
    )


@app.route('/create_tender',methods=['GET','POST'])
@token_required
def create_tender():
    if request.method=='GET':
        print(g.user)
        return render_template(
        'create_tender.html'
        )
    if request.method=='POST':
        data = request.form
        # TODO:: have to add auth and connect tender to profile
        tender_id = db.create_tender(title=data['title'], description=data['description'],
                         amount=data['amount'], days=int(data['tender_days']), price=data['price'])
        user_id = request.cookies.get('token')
        # user_id = g.user[0][
        db.connect_two_nodes_by_id(g.user['id'],tender_id)
        return render_template(
        'create_tender.html'
        )

@app.route('/sign_up',methods=['GET','POST'])
def sign_up():
    if request.method=='GET':
        return render_template(
        'sign_up.html'
        )
    if request.method=="POST":
        data = request.form
        user = db.search_single_user_phone_number(data['phone_number'])
        if len(user)==0:
            user_id = db.create_user(data['phone_number'],data['password'])
            resp = make_response(render_template("index.html"))
            resp.set_cookie('token',user_id[0]['n']['id'])
            # TODO:: have to return to home page
            return resp
        else:
            error = "این شماره همراه در حال حاظر در بانک اطلاعاتی موجود میباشد"
            print (error)
            return render_template(
            'sign_up.html',error=error
            )

        return render_template(
        'sign_up.html'
        )
@app.route('/login',methods=['GET','POST'])
def log_in():
    if request.method=="GET":
        return render_template(
        "log_in.html"
        )
    if request.method=="POST":
        data = request.form
        user = db.search_single_user_phone_number(data['phone_number'])
        print(user)
        print(len(user))
        if len(user)>0:
            if user.get("password") == data['password']:
                print("we are set")
                resp = make_response(render_template("index.html"))
                resp.set_cookie('token',user.get('id'))
                return resp
        error = "شماره همراه یا رمز عبور وارد شده درست نمیاشد، لطفا دوباره تلاش کنید"
        return render_template(
        "log_in.html", error = error
        )


@app.route('/tenders',methods=['GET','POST'])
@token_required
def get_tenders():
    if request.method=="GET":
        user = g.user
        tenders = db.show_user_tenders(g.user['id'])
        print(tenders)
        return render_template(
        "tender.html", tenders=tenders
        )

@app.route('/tender/<tender_id>', methods=["GET","POST"])
def get_tender(tender_id):
    if request.method =="GET":
        my_tender = db.get_single_tender(tender_id)

        if len(my_tender) > 0:
            my_tender = my_tender[0]
        else:
            error = "این اگهی وجود ندارد" + " یا تاریخ ان تمام شده"
            my_tender = {}
            my_tender['tender']={
            "title":"unkown",
            "description":"unkown",
            "price":0,
            }
            my_tender['user']={
            "phone_number":00000
            }
            my_tender['exp_date']="تاریخ انقضا تمام شده"
            return render_template(
            "details.html", tender=my_tender, error=error,owner_page = False
            )

        my_cookie = request.cookies.get('token')
        owner_page=False
        if my_tender['user']['id']==my_cookie:
            owner_page=True
        if not my_cookie:
            my_tender['user']['phone_number'] = "برای دیدن شماره همراه باید وارد حساب کاربری خود بشوید"
        error = None
        return render_template(
        "details.html", tender=my_tender, error=error,owner_page = owner_page
        )
    if request.method == 'POST':
        print("we are here")
        my_tender = db.get_single_tender(tender_id)
        my_cookie = request.cookies.get('token')
        if   len(my_tender) > 0 and my_tender[0]['user']['id']==my_cookie:
            db.deactivate_tender(tender_id)
            tenders = db.show_user_tenders(my_cookie)
            print(tenders)
            return render_template(
            "tender.html", tenders=tenders
            )
        return render_template(
        "index.html"
        )

@app.route('/test', methods=["GET"])
def test():
    if request.method =="GET":
        return render_template(
        "details.html"
        )
@app.route('/public_tenders', methods=["GET"])
def public_tenders():
    if request.method=="GET":
        if request.args.get("skip"):
            skip = request.args.get("skip")
        else:
            skip = 0
        if request.args.get("limit"):
            limit = request.args.get('limit')
        else:
            limit = 10
        tenders = db.get_tenders(skip=skip, limit=limit)
        return render_template(
        "tender.html",tenders=tenders
        )



@app.route('/auction',methods=['GET',"POST"])
@token_required
def create_auctions():
    if request.method=="GET":
        return render_template("create_auction.html")
    if request.method == "POST":
        data = request.form
        tender_id = db.create_auction(title=data['title'], description=data['description'],
                         amount=data['amount'], days=int(data['tender_days']), price=data['price'],node_type="auction")
        user_id = g.user
        db.connect_two_nodes_by_id(g.user['id'],tender_id)
        return render_template('create_auction.html')

if __name__=="__main__":
    app.run(debug=True)
